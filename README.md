# README

## EXAMPLE 

```php
$badRequest = [
    'code'=>'BAD_REQUEST',
    'message'=>'',
    'data'=>[
        'login'=>[
            'Value cannot be blank'
        ],
        'password'=>[
            'Value cannot be blank'
        ],
    ]
];

ApidocBuilder::$outputDir = __DIR__;

ApidocBuilder::getInstance('POST', '/user/auth', 'User')
    ->setTitle('Авторизация')
    ->setRequest(['login'=>'test@gmail.com', 'password'=>'a1b2c3'], 'Авторизация в системе', [], [
        new FieldSettings('login', 'String', 'Логин пользователя'),
        new FieldSettings('password', 'String', 'Пароль пользователя')
    ])
    ->setSuccessResponse(['code'=>'SUCCESS', 'message'=>'', 'data'=>['token'=>'0fca91128f89b883357ed335e3305e94']], 'Успешный ответ', 200, [
        new FieldSettings('code', 'String', 'Код сервера'),
        new FieldSettings('message', 'String', 'Сообщение сервера'),
        new FieldSettings('data', 'Object', 'Дополнительные данные'),
        new FieldSettings('data[token]', 'String', 'Авторизационный токен'),
    ])
    ->setErrorResponse($badRequest, 'Ошибка (BAD_REQUEST)', 400, [
        new FieldSettings('code', 'String', 'Код сервера', false, null),
        new FieldSettings('message', 'String', 'Сообщение сервера',  false, null),
        new FieldSettings('data', 'Object', 'Дополнительные данные', false, null),
        new FieldSettings('data[login]', 'String[]', 'Список ошибок связанных с неправильным заполнением поля', false, null),
        new FieldSettings('data[password]', 'String[]', 'Список ошибок связанных с неправильным заполнением поля', false, null),
    ], true, true)
    ->setErrorResponse(['code'=>'USER_NOT_FOUND', 'message'=>'', 'data'=>[]], 'Ошибка (USER_NOT_FOUND)', 400, [
        new FieldSettings('code', 'String', 'Код сервера', false, null),
        new FieldSettings('message', 'String', 'Сообщение сервера',  false, null),
        new FieldSettings('data', 'Object', 'Дополнительные данные', false, null),
    ], true, true)
    ->output();
```

## Output 

```php
<?php 
/**
* @api {post} /user/auth Авторизация
* @apiVersion 1.0.0
* @apiGroup User
* 
* 
* @apiExample {curl} Авторизация в системе 
*     curl --request POST \
*          --data '{"login":"test@gmail.com","password":"a1b2c3"}' \
*          http://127.0.0.1/user/auth
* 
* @apiParam  {String} login Логин пользователя
* @apiParam  {String} password Пароль пользователя
* 
* 
* @apiSuccess (Успешный ответ) {String} code Код сервера
* @apiSuccess (Успешный ответ) {String} message Сообщение сервера
* @apiSuccess (Успешный ответ) {Object} data Дополнительные данные
* @apiSuccess (Успешный ответ) {String} data[token] Авторизационный токен
* 
* 
* @apiError (Ошибка (BAD_REQUEST)) {String} code Код сервера
* @apiError (Ошибка (BAD_REQUEST)) {String} message Сообщение сервера
* @apiError (Ошибка (BAD_REQUEST)) {Object} data Дополнительные данные
* @apiError (Ошибка (BAD_REQUEST)) {String[]} data[login] Список ошибок связанных с неправильным заполнением поля
* @apiError (Ошибка (BAD_REQUEST)) {String[]} data[password] Список ошибок связанных с неправильным заполнением поля
* 
* 
* @apiError (Ошибка (USER_NOT_FOUND)) {String} code Код сервера
* @apiError (Ошибка (USER_NOT_FOUND)) {String} message Сообщение сервера
* @apiError (Ошибка (USER_NOT_FOUND)) {Object} data Дополнительные данные
* 
* 
* @apiSuccessExample Успешный ответ
*     HTTP/1.1 200 OK
*     {
*       "code": "SUCCESS",
*       "message": "",
*       "data": {
*         "token": "0fca91128f89b883357ed335e3305e94"
*       }
*     }
* @apiErrorExample Ошибка (BAD_REQUEST)
*     HTTP/1.1 400 BAD REQUEST
*     {
*       "code": "BAD_REQUEST",
*       "message": "",
*       "data": {
*         "login": [
*           "Value cannot be blank"
*         ],
*         "password": [
*           "Value cannot be blank"
*         ]
*       }
*     }
* @apiErrorExample Ошибка (USER_NOT_FOUND)
*     HTTP/1.1 400 BAD REQUEST
*     {
*       "code": "USER_NOT_FOUND",
*       "message": "",
*       "data": [
*         
*       ]
*     }
*/
```
