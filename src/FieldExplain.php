<?php

namespace devbox741\apidoc\gen;

/**
 * Информация о свойстве объекта
 */
class FieldExplain
{

    const TYPE_STRING =  'String';
    const TYPE_NUMBER =  'Number';
    const TYPE_BOOLEAN =  'Boolean';
    const TYPE_OBJECT =  'Object';
    const TYPE_STRING_ARRAY =  'String[]';
    const TYPE_FLOAT =  'Float';
    const TYPE_ARRAY =  'Array';

    /**
     * Наименование свойства
     * (вложенные данные можно указать через точку, например: invoice.number)
     * @var string
     */
    private $name;

    /**
     * Тип свойства
     * (String, Number, Boolean, Object, String[] и т.д.)
     * @var string
     */
    private $type;

    /**
     * Значение c может принимать значение NULL
     * @var bool
     */
    private $optional = false;

    /**
     * Описание свойства
     * @var string
     */
    private $description;

//    /**
//     * Группа
//     * @var string
//     */
//    private $group = '';

    /**
     * Значение свойства по умолчанию
     * @var string
     */
    private $defaultValue;

    /**
     * @param string $name
     * @param string $type
     * @param string $description
     * @param array $config
     */
    public function __construct(string $name, string $type='String', string $description='', array $config=[]){
        $this->name = $name;
        $this->type = $type;
        $this->description = $description;
        $this->optional = !empty($config['optional']);
        $this->defaultValue = $config['defaultValue']??'';
        $this->group = $config['group']??'';
    }

    public static function create(array $config): self{
        $obj = new self($config['name']);
        foreach ($config as $k => $v){
            $obj->$k = $v;
        }
        return $obj;
    }

    /**
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @param bool $optional
     * @return FieldExplain
     */
    public function setOptional(bool $optional): FieldExplain
    {
        $this->optional = $optional;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description??'';
    }

    /**
     * @param string $description
     * @return FieldExplain
     */
    public function setDescription(string $description): FieldExplain
    {
        $this->description = $description;
        return $this;
    }

//    /**
//     * @return string
//     */
//    public function getGroup(): string
//    {
//        return $this->group;
//    }
//
//    /**
//     * @param string $group
//     * @return FieldExplain
//     */
//    public function setGroup(string $group): FieldExplain
//    {
//        $this->group = $group;
//        return $this;
//    }

    /**
     * @return string|null
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param string $defaultValue
     * @return FieldExplain
     */
    public function setDefaultValue(string $defaultValue): FieldExplain
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FieldExplain
     */
    public function setName(string $name): FieldExplain
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return FieldExplain
     */
    public function setType(string $type): FieldExplain
    {
        $this->type = $type;
        return $this;
    }

}