<?php

namespace devbox741\apidoc\gen;

/**
 * Информация о запросе к API
 */
class Request
{

    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';
    const METHOD_PUT = 'PUT';

    /**
     * @var string
     */
    private $host = 'http://127.0.0.1';

    /**
     * Тип запроса (пример: GET, POST, PATCH, DELETE, PUT)
     * @var string
     */
    private $method;

    /**
     * URL адрес
     * @var string
     */
    private $path;

    /**
     * Название блока в документации
     * (например: Данные о пользователе)
     * @var string
     */
    private $name='';

    /**
     * Описание блока в документации
     * @var string
     */
    private $description='';

    /**
     * Наименование запроса
     * @var string
     */
    private $title='';

    /**
     * Версия API
     * @var string
     */
    private $apiVersion = '1.0.0';

    /**
     * Доступ к методу
     * @var string
     */
    private $permission='';

    /**
     * Раздел в API в котором отражать данные запроса
     * @var string
     */
    private $group='';

    /**
     * Массив с параметрами запроса
     * @var array
     */
    private $paramFields = [];

    /**
     * Информация о передаваемых данных
     * @var FieldExplain[]
     */
    private $paramFieldExplains = [];

    /**
     * Переданные данные
     * @var array
     */
    private $bodyFields=[];

    /**
     * Информация о переданных данных
     * @var FieldExplain[]
     */
    private $bodyFieldExplains=[];

    /**
     * Отправленные заголовки
     * @var array
     */
    private $headers = [];

    /**
     * Информация об отправленных заголовках
     * @var FieldExplain[]
     */
    private $headerExplains = [];

    public function __construct(array $param){
        $this->method = $param['method'];
        $this->path = $param['path'];
        $this->name = $param['name'];
        $this->group = $param['group'];

        $this->permission = $param['permission']??'';
        $this->headers = $param['headers']??[];
        $this->bodyFields = $param['bodyFields']??[];
        $this->paramFields = $param['paramFields']??[];
        $this->title = $param['title']??'';
        $this->description = $param['description']??'';
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return Request
     */
    public function setMethod(string $method): Request
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Request
     */
    public function setPath(string $path): Request
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Request
     */
    public function setName(string $name): Request
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description??'';
    }

    /**
     * @param string $description
     * @return Request
     */
    public function setDescription(string $description): Request
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiVersion(): string
    {
        return $this->apiVersion;
    }

    /**
     * @param string $apiVersion
     * @return Request
     */
    public function setApiVersion(string $apiVersion): Request
    {
        $this->apiVersion = $apiVersion;
        return $this;
    }

    /**
     * @return string
     */
    public function getPermission(): string
    {
        return $this->permission;
    }

    /**
     * @param string $permission
     * @return Request
     */
    public function setPermission(string $permission): Request
    {
        $this->permission = $permission;
        return $this;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @param string $group
     * @return Request
     */
    public function setGroup(string $group): Request
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return array
     */
    public function getParamFields(): array
    {
        return $this->paramFields;
    }

    /**
     * @param array $paramFields
     * @return Request
     */
    public function setParamFields(array $paramFields): Request
    {
        $this->paramFields = $paramFields;
        return $this;
    }

    /**
     * @return FieldExplain[]
     */
    public function getParamFieldExplains(): array
    {
        return $this->paramFieldExplains;
    }

    /**
     * @param FieldExplain $config
     * @return Request
     */
    public function addParamFieldExplain(FieldExplain $config): Request
    {
        $this->paramFieldExplains[] = $config;
        return $this;
    }

    /**
     * @return array
     */
    public function getBodyFields(): array
    {
        return $this->bodyFields;
    }

    /**
     * @param array $bodyFields
     * @return Request
     */
    public function setBodyFields(array $bodyFields): Request
    {
        $this->bodyFields = $bodyFields;
        return $this;
    }

    /**
     * @return FieldExplain[]
     */
    public function getBodyFieldExplains(): array
    {
        return $this->bodyFieldExplains;
    }

    /**
     * @param FieldExplain $bodyFieldExplains
     * @return Request
     */
    public function addBodyFieldExplain(FieldExplain $bodyFieldExplains): Request
    {
        $this->bodyFieldExplains[] = $bodyFieldExplains;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     * @return Request
     */
    public function setHeaders(array $headers): Request
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return FieldExplain[]
     */
    public function getHeaderExplains(): array
    {
        return $this->headerExplains;
    }

    /**
     * @param FieldExplain $explain
     * @return Request
     */
    public function addHeaderExplain(FieldExplain $explain): Request
    {
        $this->headerExplains[] = $explain;
        return $this;
    }

    /**
     * @param FieldExplain[] $explains
     * @return Request
     */
    public function setHeaderExplains(array $explains): Request
    {
        $this->headerExplains[] = $explains;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Request
     */
    public function setTitle(string $title): Request
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return Request
     */
    public function setHost(string $host): Request
    {
        $this->host = $host;
        return $this;
    }

}