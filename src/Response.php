<?php

namespace devbox741\apidoc\gen;

class Response
{

    /**
     * Заголовок
     * @var string
     */
    private $title='';

    /**
     * Полученные от сервера данные
     * @var array
     */
    private $fields = [];

    /**
     * Дополнительная информация о полученных данных
     * @var FieldExplain[]
     */
    private $fieldsExplain = [];

    /**
     * HTTP код ответа
     * @var int
     */
    private $httpCode;

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return Response
     */
    public function setFields(array $fields): Response
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return FieldExplain[]
     */
    public function getFieldsExplain(): array
    {
        return $this->fieldsExplain;
    }

    /**
     * @param FieldExplain $fieldExplain
     * @return Response
     */
    public function addFieldExplain(FieldExplain $fieldExplain): Response
    {
        $this->fieldsExplain[] = $fieldExplain;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Response
     */
    public function setTitle(string $title): Response
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @param int $httpCode
     * @return Response
     */
    public function setHttpCode(int $httpCode): Response
    {
        $this->httpCode = $httpCode;
        return $this;
    }

}