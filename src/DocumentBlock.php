<?php

namespace devbox741\apidoc\gen;

class DocumentBlock
{

    /**
     * Корректный запрос к API
     * В документацию будет добавлена таблица с описанием всех параметров запроса
     * @var Request
     */
    private $mainRequest;

    /**
     * Пример успешного запроса к API
     * В документацию будет добавлен пример запроса к серверу
     * @var Request
     */
    private $exampleRequest;

    /**
     * Пример успешных ответов от сервера
     * В документацию будет добавлена таблица с описанием всех параметров ответа
     * @var Response[]
     */
    private $successResponses = [];

    /**
     * Пример всех успешных ответов от сервера
     * В документацию будет добавлен JSON с данными полученными с сервера
     * @var Response[]
     */
    private $exampleSuccessResponses = [];

    /**
     * Пример неуспешных ответов от сервера
     * В документацию будет добавлена таблица с описанием всех параметров ответа
     * @var Response[]
     */
    private $errorResponses = [];

    /**
     * Пример всех неуспешных ответов от сервера
     * В документацию будет добавлен JSON с данными полученными с сервера
     * @var Response[]
     */
    private $exampleErrorResponses = [];

    public function __construct(Request $request){
        $this->mainRequest = $request;
    }

    /**
     * @return Request
     */
    public function getMainRequest(): Request
    {
        return $this->mainRequest;
    }

    /**
     * @param Request $mainRequest
     * @return DocumentBlock
     */
    public function setMainRequest(Request $mainRequest): DocumentBlock
    {
        $this->mainRequest = $mainRequest;
        return $this;
    }

    /**
     * @return Request
     */
    public function getExampleRequest(): Request
    {
        return $this->exampleRequest;
    }

    /**
     * @param Request $exampleRequest
     * @return DocumentBlock
     */
    public function setExampleRequest(Request $exampleRequest): DocumentBlock
    {
        $this->exampleRequest = $exampleRequest;
        return $this;
    }

    /**
     * @return Response[]
     */
    public function getSuccessResponses(): array
    {
        return $this->successResponses;
    }

    /**
     * @param Response $successResponse
     * @return DocumentBlock
     */
    public function appendSuccessResponse(Response $successResponse): DocumentBlock
    {
        $this->successResponses[] = $successResponse;
        return $this;
    }

    /**
     * @return Response[]
     */
    public function getExampleSuccessResponses(): array
    {
        return $this->exampleSuccessResponses;
    }

    /**
     * @param Response $exampleSuccessResponses
     * @return DocumentBlock
     */
    public function appendExampleSuccessResponses(Response $exampleSuccessResponses): DocumentBlock
    {
        $this->exampleSuccessResponses[] = $exampleSuccessResponses;
        return $this;
    }

    /**
     * @return Response[]
     */
    public function getExampleErrorResponses(): array
    {
        return $this->exampleErrorResponses;
    }

    /**
     * @param Response $exampleErrorResponse
     * @return DocumentBlock
     */
    public function appendExampleErrorResponses(Response $exampleErrorResponse): DocumentBlock
    {
        $this->exampleErrorResponses[] = $exampleErrorResponse;
        return $this;
    }

    /**
     * @return Response[]
     */
    public function getErrorResponses(): array
    {
        return $this->errorResponses;
    }

    /**
     * @param Response $errorResponse
     * @return DocumentBlock
     */
    public function appendErrorResponse(Response $errorResponse): DocumentBlock
    {
        $this->errorResponses[] = $errorResponse;
        return $this;
    }

    public function render(): string {
        $output = [];
        $output[] = sprintf('@api {%s} %s %s', $this->mainRequest->getMethod(), $this->mainRequest->getPath(), $this->mainRequest->getTitle());
        $output[] = sprintf('@apiVersion %s', $this->mainRequest->getApiVersion());
        $output[] = sprintf('@apiName %s', $this->mainRequest->getName());
        $output[] = sprintf('@apiGroup %s', $this->mainRequest->getGroup());
        if (!empty($this->mainRequest->getPermission())) $output[] = sprintf('@apiPermission %s', $this->mainRequest->getPermission());
        if (!empty($this->mainRequest->getDescription())) $output[] = sprintf('@apiDescription %s', $this->mainRequest->getDescription());

        $output = array_merge($output,  $this->headers($this->mainRequest));
        $output = array_merge($output, $this->params($this->mainRequest));
        $output = array_merge($output, $this->body($this->mainRequest));

        if (!empty($this->exampleRequest)){
            $output[] = '';
            $output = array_merge($output, $this->apiCurlExample($this->exampleRequest));
        }

        foreach ($this->successResponses as $successResponse){
            $output = array_merge($output, $this->success($successResponse));
        }

        foreach ($this->exampleSuccessResponses as $exampleResponse){
            $output = array_merge($output, $this->apiSuccessExample($exampleResponse));
        }

        foreach ($this->errorResponses as $errorResponse){
            $output = array_merge($output, $this->error($errorResponse));
        }

        foreach ($this->exampleErrorResponses as $exampleResponse){
            $output = array_merge($output, $this->apiErrorExample($exampleResponse));
        }

        $result = [];
        $result[] = $this->asComment($output);
        $result[] = sprintf('public function %s(){}', md5($this->mainRequest->getMethod().$this->mainRequest->getPath().$this->mainRequest->getTitle()));

        return join("\n", $result);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function headers(Request $request): array {
        return $this->fields($request->getHeaders(), $request->getHeaderExplains(), '@apiHeader', 'Заголовки');
    }

    /**
     * @param Request $request
     * @return array
     */
    private function params(Request $request): array {
        return $this->fields($request->getParamFields(), $request->getParamFieldExplains(), '@apiParam', 'Параметры запроса');
    }

    /**
     * @param Request $request
     * @return array
     */
    private function body(Request $request): array {
        return $this->fields($request->getBodyFields(), $request->getBodyFieldExplains(), '@apiBody', 'Тело запроса');
    }

    /**
     * @param Response $response
     * @return array
     */
    private function success(Response $response): array {
        return $this->fields($response->getFields(), $response->getFieldsExplain(), '@apiSuccess', $response->getTitle());
    }

    /**
     * @param Response $response
     * @return array
     */
    private function error(Response $response): array {
        return $this->fields($response->getFields(), $response->getFieldsExplain(), '@apiError', $response->getTitle());
    }

    /**
     * @param Response $response
     * @return array
     */
    private function apiSuccessExample(Response $response): array {
        return $this->apiExample('@apiSuccessExample', $response, $response->getHttpCode());
    }

    /**
     * @param Response $response
     * @return array
     */
    private function apiErrorExample(Response $response): array {
        return $this->apiExample('@apiErrorExample', $response, $response->getHttpCode());
    }

    /**
     * @param Request $request
     * @return array
     */
    private function apiCurlExample(Request $request): array {
        $result = [];
        $result[] = sprintf('@apiExample {bash} %s', $request->getTitle());

        $curl = [];
        $curl[] = sprintf('curl -X %s', mb_strtoupper($request->getMethod()));
        foreach ($request->getHeaders() as $hk=>$hv){
            $curl[] = sprintf("    -H '%s: %s'", $hk, $hv);
        }
        if (!empty($request->getBodyFields())){
            $json = strtr(json_encode($request->getBodyFields(), JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT), ["'"=>"\'"]);
            foreach (explode("\n", $json) as $line){
                if ($line==='{') $curl[] = '    -d \'{';
                elseif($line==='}') $curl[] = '    }\'';
                else $curl[] = '    '.$line;
            }
        }

        $url = $request->getHost().$request->getPath();
        if (!empty($request->getParamFields())) $url .= '?'.http_build_query($request->getParamFields());

        $curl[] = sprintf("    %s", $url);

        foreach ($curl as $item){
            $result[] = $item;
        }

        $result[] = '';

        return $result;
    }

    /**
     * @param string $tag
     * @param Response $response
     * @param int|mixed $httpCode
     * @return array
     */
    private function apiExample(string $tag, Response $response, $httpCode = null): array {
        $result = [];

        $result[] = sprintf('%s {json} %s', $tag, $response->getTitle());

        if (!empty($httpCode)) {
            $result[] = sprintf('    HTTP/1.1 %s %s', $httpCode, $this->getHttpCodeDescription($httpCode));
        }

        foreach (explode("\n", json_encode($response->getFields(), JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT)) as $line){
            $result[] = '    '.$line;
        }

        $result[] = '';
        return $result;
    }

    /**
     * @param array $fields
     * @param FieldExplain[] $fieldsConfig
     * @param string $tag
     * @return array
     */
    private function fields(array $fields, array $fieldsConfig, string $tag, string $group = null): array {
        $fieldsHash = $this->getFieldsHash($fields);
        $fieldsConfigHash = $this->getFieldsConfigHash($fieldsConfig);

        $groupExt = empty($group)?$group:'('.$group.')';

        $result = [];
        foreach ($fieldsHash as $name => $value){
            if (isset($fieldsConfigHash[$name])) {
                $type = $fieldsConfigHash[$name]->getType();
                $defaultValue = $fieldsConfigHash[$name]->getDefaultValue();
                $description = $fieldsConfigHash[$name]->getDescription();
                $optional = $fieldsConfigHash[$name]->isOptional();
            }else{
                $type = (is_array($value) || preg_match('/\[\]$/', $name))?'Array':'Unknown';
                $defaultValue = '';
                $description = '';
                $optional = false;
            }

            $nameExtended = (!empty($defaultValue)?$name.'='.$defaultValue:$name);
            $apiParamName = $optional?'['.$nameExtended.']':$nameExtended;

            $apiParam = sprintf('%s %s {%s} %s %s', $tag, $groupExt, $type, $apiParamName, $description);
            $apiParam = preg_replace('!\s+!' , ' ', $apiParam);

            $result[] = $apiParam;
        }

        $result[] = '';

        return $result;
    }

    /**
     * @param array $fields
     * @param string $prefix
     * @return array
     */
    private function getFieldsHash(array $fields, string $prefix = ''): array {
        $result = [];
        foreach ($fields as $name => $value){
            if(is_array($value) && is_numeric(key($value)) && is_array($value[key($value)])) {
                if (empty($prefix)) $nestedPrefix = $name;
                else $nestedPrefix = $prefix.'.'.$name;

                $result[$nestedPrefix] = '';

                foreach ($value as $nested){
                    foreach ($this->getFieldsHash($nested, $nestedPrefix) as $k=>$v){
                        $result[$k] = $v;
                    }
                }
            }
            elseif (is_array($value) && is_string(key($value))){
                if (empty($prefix)) $nestedPrefix = $name;
                else $nestedPrefix = $prefix.'.'.$name;

                $result[$nestedPrefix] = '';

                $result = $result + $this->getFieldsHash($value, $nestedPrefix);
            }
            else{
                if (!empty($prefix)) $newName = $prefix.'.'.$name;
                else $newName = $name;

                $result[$newName] = $value;
            }
        }
        return $result;
    }

    /**
     * @param FieldExplain[] $fieldsConfig
     * @return FieldExplain[]
     */
    private function getFieldsConfigHash(array $fieldsConfig): array {
        $result = [];
        foreach ($fieldsConfig as $fieldConfig){
            $result[$fieldConfig->getName()] = $fieldConfig;
        }
        return $result;
    }

    private function asComment(array $lines): string {
        $output = ['/**'];
        foreach ($lines as $line){
            $output[] = ' * '.$line;
        }
        $output[] = ' */';
        return join("\n", $output);
    }

    private function getHttpCodeDescription(string $httpCode): string {
        switch ($httpCode) {
            case 100: $text = 'Continue'; break;
            case 101: $text = 'Switching Protocols'; break;
            case 200: $text = 'OK'; break;
            case 201: $text = 'Created'; break;
            case 202: $text = 'Accepted'; break;
            case 203: $text = 'Non-Authoritative Information'; break;
            case 204: $text = 'No Content'; break;
            case 205: $text = 'Reset Content'; break;
            case 206: $text = 'Partial Content'; break;
            case 300: $text = 'Multiple Choices'; break;
            case 301: $text = 'Moved Permanently'; break;
            case 302: $text = 'Moved Temporarily'; break;
            case 303: $text = 'See Other'; break;
            case 304: $text = 'Not Modified'; break;
            case 305: $text = 'Use Proxy'; break;
            case 400: $text = 'Bad Request'; break;
            case 401: $text = 'Unauthorized'; break;
            case 402: $text = 'Payment Required'; break;
            case 403: $text = 'Forbidden'; break;
            case 404: $text = 'Not Found'; break;
            case 405: $text = 'Method Not Allowed'; break;
            case 406: $text = 'Not Acceptable'; break;
            case 407: $text = 'Proxy Authentication Required'; break;
            case 408: $text = 'Request Time-out'; break;
            case 409: $text = 'Conflict'; break;
            case 410: $text = 'Gone'; break;
            case 411: $text = 'Length Required'; break;
            case 412: $text = 'Precondition Failed'; break;
            case 413: $text = 'Request Entity Too Large'; break;
            case 414: $text = 'Request-URI Too Large'; break;
            case 415: $text = 'Unsupported Media Type'; break;
            case 500: $text = 'Internal Server Error'; break;
            case 501: $text = 'Not Implemented'; break;
            case 502: $text = 'Bad Gateway'; break;
            case 503: $text = 'Service Unavailable'; break;
            case 504: $text = 'Gateway Time-out'; break;
            case 505: $text = 'HTTP Version not supported'; break;
            default:
                $text = '';
                break;
        }

        return $text;
    }

}