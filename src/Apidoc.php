<?php

namespace devbox741\apidoc\gen;

use Symfony\Component\Yaml\Yaml;

class Apidoc
{

    /**
     * @var string
     */
    public static $host = 'http://127.0.0.1';

    /**
     * @var array
     */
    public static $schema = [];

    /**
     * @var array
     */
    public static $routes=[];

    /**
     * @param string $routeId
     * @param string|null $schemaId
     * @param array $paramFields
     * @param array $bodyFields
     * @param array $headerFields
     * @param FieldExplain[] $headerExplains
     * @return Request
     * @throws \Exception
     */
    public static function createRequest(string $routeId, string $schemaId=null,  array $bodyFields=[], array $paramFields=[],  array $headerFields=[], array $headerExplains=[]): Request {
        $routes = self::readRoutes();
        $schema = self::readSchema();

        if (!isset($routes[$routeId])){
            throw new \Exception(sprintf('Route %s not found', $routeId));
        }

        $request = new Request($routes[$routeId]);
        $request->setHost(self::$host);
        $request->setHeaders($headerFields);
        $request->setBodyFields($bodyFields);
        $request->setParamFields($paramFields);

        foreach (self::getSchemaExtended($schemaId, $schema) as $k => $v){
            $config = $v;
            $config['name'] = $k;

            $request->addParamFieldExplain(FieldExplain::create($config));
            $request->addBodyFieldExplain(FieldExplain::create($config));
        }

        foreach ($headerExplains as $v){
            $request->addHeaderExplain($v);
        }

        return $request;
    }

    /**
     * @param array $fields
     * @param int $httpCode
     * @param string|null $schemaId
     * @param array $options
     * @return Response
     */
    public static function createResponse(array $fields=[], int $httpCode=200, string $schemaId=null, array $options=[]): Response {
        $response = new Response();
        $response->setFields($fields);
        $response->setHttpCode($httpCode);

        foreach (self::getSchemaExtended($schemaId, self::readSchema()) as $k=> $v){
            $config = $v;
            $config['name'] = $k;
            $response->addFieldExplain(FieldExplain::create($config));
        }

        foreach ($options as $k=>$v){
            switch ($k){
                case 'title':
                    $response->setTitle($v);
                    break;
            }
        }

        return $response;
    }

    public static function getSchema(): array{
        return self::readSchema();
    }

    public static function getSchemaExtended(string $type, array $schema, string $prefix='', bool $is_array = false){
        if (!isset($schema[$type])) {
            return [];
        }

        if ($is_array) {
            $prefix = ($prefix??'Object');
        }

        $result = [];
        foreach ($schema[$type] as $k => $v){
            $vType = $v['type']??'String';
            $isArray = ($v['is_array'] || $vType === 'Array');

            if (isset($schema[$vType])){
                $nestedPrefix = empty($prefix)?$k:($prefix.'.'.$k);
                $result[$nestedPrefix] = $v;
                $result = $result + self::getSchemaExtended($vType, $schema, $nestedPrefix, $isArray??false);
            }else{
                if (!empty($prefix)){
                    $result[sprintf('%s.%s', $prefix, $k)] = $v;
                }else{
                    $result[$k] = $v;
                }
            }
        }

        return $result;
    }

    public static function render(DocumentBlock $docBlock): string {
        return $docBlock->render();
    }

    /**
     * @param $schemaId
     * @return FieldExplain[]
     */
    public static function getExplainById($schemaId): array {
        $explains = [];
        foreach (self::getSchemaExtended($schemaId, self::readSchema()) as $k=> $v){
            $config = $v;
            $config['name'] = $k;
            $explains[] = FieldExplain::create($config);
        }
        return $explains;
    }

    private static function readSchema(): array {
        $result = [];
        foreach (self::$schema as $filename){
            $parsed = Yaml::parseFile($filename);
            foreach ($parsed['schema'] as $k=>$v){
                $result[$k] = $v;
            }
        }
        return $result;
    }

    private static function readRoutes(): array {
        $result = [];
        foreach (self::$routes as $filename){
            $parsed = Yaml::parseFile($filename);
            foreach ($parsed['routes'] as $k=>$v){
                $result[$k] = $v;
            }
        }
        return $result;
    }

}