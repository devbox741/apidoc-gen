<?php

declare(strict_types=1);

namespace devbox741\apidoc\gen\tests;

use devbox741\apidoc\gen\Apidoc;
use devbox741\apidoc\gen\DocumentBlock;
use devbox741\apidoc\gen\FieldExplain;

class FirstTest extends ApidocTestCase
{

    public static function getAuthHeaders(): array {
        return [
            'Content-Type'=>'application/json',
            'Authorization'=>'e10adc3949ba59abbe56e057f20f883e'
        ];
    }

    public static function getAuthHeadersExplain(): array {
        return [
            new FieldExplain('Content-Type', 'String', 'MIME тип ресурса'),
            new FieldExplain('Authorization', 'String', 'Авторизационный токен'),
        ];
    }

    public function testExample2(){
        $this->assertTrue(1 === 1);
    }

    /**
     * @throws \Exception
     */
    public function testExample()
    {
        $res = Apidoc::getExplainById('Invoice');

        $mainRequest = Apidoc::createRequest('invoice-create', 'Invoice', [
            'legal_entity_id' => 103,
            'items'=>[
                ['name'=>'Холодильник Side by Side Bosch KAG93AI30R', 'quantity'=>1, 'price'=>38100.00],
                ['name'=>'Антивибрационные подставки Electrolux E4WHPA 02 9029795243', 'quantity'=>4, 'price'=>121.00]
            ]
        ], [], self::getAuthHeaders(), self::getAuthHeadersExplain());

        $successResponse = Apidoc::createResponse([
            'id'=>'1',
            'number'=>'A000000001',
            'sum'=>38584.00,
            'items'=>[
                ['name'=>'Холодильник Side by Side Bosch KAG93AI30R', 'quantity'=>1, 'price'=>38100.00],
                ['name'=>'Антивибрационные подставки Electrolux E4WHPA 02 9029795243', 'quantity'=>4, 'price'=>121.00]
            ],
            'legal_entity_id'=>103,
            'created_at'=>'2021-01-01 09:01:32',
            'updated_at'=>'2021-01-01 09:01:32',
        ], 200, 'Invoice', ['title'=>'Ответ сервера после создания счёта']);

        $errorResponse = Apidoc::createResponse([
            'code'=>'BAD_REQUEST',
            'data'=>[
                'errors'=>[
                    'legal_entity_id'=>[
                        'Юридическое лицо не найдено'
                    ],
                    'items' => [
                        'В счёте должна быть хотя бы одна позиция'
                    ]
                ]
            ]
        ], 400, 'BadRequestResponse', ['title'=>'Ответ сервера в случае ошибки'])
        ->addFieldExplain(new FieldExplain('data.errors', 'Object', 'Список ошибок'))
        ->addFieldExplain(new FieldExplain('data.errors.legal_entity_id', 'String[]', 'Список ошибок'))
        ->addFieldExplain(new FieldExplain('data.errors.items', 'String[]', 'Список ошибок'));

        $docBlock = new DocumentBlock($mainRequest);
        $docBlock->setExampleRequest((clone $mainRequest)->setTitle('Пример успешного запроса'));
        $docBlock->appendSuccessResponse($successResponse);
        $docBlock->appendErrorResponse($errorResponse);
        $docBlock->appendExampleSuccessResponses($successResponse);
        $docBlock->appendExampleErrorResponses($errorResponse);

        file_put_contents(__DIR__.'/../runtime/docs/src/api.php', $docBlock->render());

        $this->assertTrue(1===1);
    }

}