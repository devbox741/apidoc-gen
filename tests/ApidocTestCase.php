<?php

declare(strict_types=1);

namespace devbox741\apidoc\gen\tests;

use devbox741\apidoc\gen\Apidoc;

class ApidocTestCase extends \PHPUnit\Framework\TestCase
{

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        Apidoc::$schema =  [__DIR__.'/schema.yml'];
        Apidoc::$routes =  [__DIR__.'/routes.yml'];
    }

}