composer-install:
	docker-compose run --rm composer composer update

run-tests:
	docker run -v $(CURDIR):/var/www -w /var/www php:7.0-cli bash -c './vendor/bin/phpunit tests --bootstrap ./tests/bootstrap.php'

apidocjs:
	docker-compose run --rm apidocjs apidoc -i src -o dist

run: run-tests apidocjs

